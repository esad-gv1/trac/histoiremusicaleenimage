import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
 
public class Fenetre extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Fenetre(){

//Fen�tre :                
    //D�finit un titre pour notre Fen�tre
    this.setTitle("TRAC : Projet Groupe 1");
    //D�finit sa taille : 400 pixels de large et 100 pixels de haut
    this.setSize(1920, 1080);
    //Nous demandons maintenant � notre objet de se positionner au centre
    this.setLocationRelativeTo(null);
    //Termine le processus lorsqu'on clique sur la croix rouge
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //Et enfin, la rendre visible        
    this.setVisible(true);
    
//Couche visible :    
  //Instanciation d'un objet JPanel
    JPanel pan = new JPanel();
    //D�finition de sa couleur de fond
    pan.setBackground(Color.RED);        
    //On pr�vient notre JFrame que notre JPanel sera son content pane
    this.setContentPane(new Panneau());
    
    
	}
}
