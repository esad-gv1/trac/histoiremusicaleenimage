import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
 
public class PanneauDynamique extends JPanel {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//conditions initiales n�gatives pour que l'image apparaisse depuis l'ext�rieur de l'�cran
	private int posX = -50;
	private int posY = -50;

  public void paintComponent(Graphics g){
	  
//Fond de la fen�tre
    //On choisit une couleur de fond pour le rectangle
    g.setColor(Color.white);
    //On le dessine de sorte qu'il occupe toute la surface
    g.fillRect(0, 0, this.getWidth(), this.getHeight());
    
    try {
        Image img = ImageIO.read(new File("Images/Babar.png"));
        g.drawImage(img, posX, posY, this);
    }catch (IOException e) {
      e.printStackTrace();
    }
  }

  public int getPosX() {
    return posX;
  }

  public void setPosX(int posX) {
    this.posX = posX;
  }

  public int getPosY() {
    return posY;
  }

  public void setPosY(int posY) {
    this.posY = posY;
  }
}