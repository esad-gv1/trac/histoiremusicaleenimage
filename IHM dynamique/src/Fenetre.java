import javax.swing.JFrame;
 
public class Fenetre extends JFrame{
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private PanneauDynamique pan = new PanneauDynamique();

  public Fenetre(){        
    this.setTitle("Babar passe");
    this.setSize(1920, 1080);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    this.setContentPane(pan);
    this.setVisible(true);
    go();
  }

  private void go(){
    for(int i = -50; i < pan.getWidth(); i++){
      int x = pan.getPosX(), y = pan.getPosY();
      x++;
      y++;
      pan.setPosX(x);
      pan.setPosY(y);
      pan.repaint();  
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }       
}
