import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
 
public class FenetreGif extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FenetreGif(){

//Fen�tre :                
    //D�finit un titre pour notre Fen�tre
    this.setTitle("Fond anim�");
    //D�finit sa taille : 1920 pixels de large et 1080 pixels de haut
    this.setSize(1920, 1080);
    //Nous demandons maintenant � notre objet de se positionner au centre
    this.setLocationRelativeTo(null);
    //Termine le processus lorsqu'on clique sur la croix rouge
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //Et enfin,fen�tre rendue visible        
    this.setVisible(true);
    
//Couche visible :    
  //Instanciation d'un objet JPanel
    JPanel pan = new JPanel();
    //D�finition de sa couleur de fond
    pan.setBackground(Color.white);        
    //On pr�vient notre JFrame que notre JPanel sera son content pane
    this.setContentPane(new PanneauGif("Images/Babar2.gif"));
    
    
	}
}
