package trac.display.graphical;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class GraphicalPanel extends JPanel {
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	// conditions initiales n�gatives pour que l'image apparaisse depuis l'ext�rieur
	// de l'�cran
	private int posX;
	private int posY;

	private String adresseImage;


	public GraphicalPanel(String adImage,Point origin,Dimension size) {
		adresseImage = adImage;
		posX = 0;
		posY = 250;
		
		this.setSize(size);
		this.setLocation(origin);

		System.out.println("[Graphical Panel] largeur " + this.getWidth() + " | hauteur " + this.getHeight());
	}

	public void paintComponent(Graphics g) {

//Fond de la fen�tre
		// On choisit une couleur de fond pour le rectangle
		g.setColor(Color.RED);
		// On le dessine de sorte qu'il occupe toute la surface
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

		try {
			g.drawImage((new ImageIcon(adresseImage)).getImage(), 0, 0, this.getWidth(), this.getHeight(), this); //

			Image img = ImageIO.read(new File("./ressources/babar/Babar.png"));
			
			g.drawImage(img, posX, posY, this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
}