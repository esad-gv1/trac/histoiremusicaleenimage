package trac.display;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import trac.text.Word;

public class Display extends JFrame{
	
	private Set<Word> words = null;

	public Display(Set<Word> words) {
		super();
		this.words = words;
		
		//initialisation des composants graphiques
		this.setTitle("mots babar");
		this.setSize(400, 1000);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(new BorderLayout());
		
		Container contentPane = this.getContentPane();

		SearchPanel searchPanel = new SearchPanel();
		WordListPanel wordListPanel = new WordListPanel(words);
		
		contentPane.add(searchPanel,BorderLayout.NORTH);
		contentPane.add(wordListPanel,BorderLayout.CENTER);
		//contentPane.add(wordListPanel);
		
		this.setVisible(true);
	}
	
	

}
