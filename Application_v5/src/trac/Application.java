package trac;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.HashSet;
import java.util.Set;

import trac.display.Frame;
import trac.text.Word;

public class Application {

	public static void main(String[] args) {
		//construction de la liste de mot et association des images et des ressources
		Set<Word> words = new HashSet<Word>();
		words.add(new Word("foret","./ressources/foret/foret.gif","./ressources/foret/foret.wav",true));
		words.add(new Word("voiture","./ressources/voiture/voiture.png","",false));
		words.add(new Word("babar","./ressources/babar/babar.png","./ressources/babar/babar.wav",false));
		words.add(new Word("ville","./ressources/ville/ville.gif","./ressources/ville/ville.wav",true));
		words.add(new Word("elephants","./ressources/elephants/elephants.png","./ressources/elephants/elephants.wav",false));
		words.add(new Word("auto","","",false));
		words.add(new Word("celeste","","",false));
		words.add(new Word("singe","","",false));
		words.add(new Word("chasseur","./ressources/chasseur/chasseur.png","./ressources/chasseur/chasseur.wav",false));
		
		//creation de la frame
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		Frame frame1 = new Frame((int)screen.getWidth(),(int)screen.getHeight());
		frame1.setVisible(true);
	}
}
